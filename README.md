# c_cpp_templete

## Setup cpputest
```
mkdir lib
cd lib
git clone git://github.com/cpputest/cpputest.git
cd cpputest/cpputest_build
autoreconf .. -i
../configure
make
```

## Build
```
mkdir build && cd build
cmake ..
make
```

## Run tests
```
./build/test/c_cpp_template_test
```

## Run
```
./build/src/c_cpp_template
```
