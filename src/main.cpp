#include <stdio.h>
#include "modular.hpp"

int main(void)
{
        printf("Hello World!\n");
        Modular<int, 10> m1(11);
        printf("m1=%d\n", m1.value());
        return 0;
}
