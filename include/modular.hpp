#ifndef MODULAR_
#define MODULAR_
#include <stdio.h>
#include <iostream>
#include <tuple>
#include <optional>

typedef long long MODULO;

template <typename T, MODULO M>
class Modular {
private:
    T value_;
    T mod(const T &val) {
        return (((val % M) + M) % M);
    }

    std::tuple<T, long long> extended_euclidean(T a, T b) {
        if(a == 0) {
            // return {b, 0, 1};
        }
        // auto [gcd, x, y] = extended_euclidean<T, M, M>(b % a, a);
        // return {gcd, y - (b/a) * x, x};
    }

    // std::optional<T> mod_inverse(T a, T m) {
    //     auto [gcd, x, y] = extended_euclidean<T, M>(a, m);
    //
    //     if(gcd != 1) {
    //         return std::nullopt;
    //     }
    //     return (x % (M)m + (M)m) % m;
    // }

public:
    Modular() { value_ = 0; };
    Modular(T value) { value_ = mod(value); };
    Modular(const Modular<T, M> &other) { value_ = mod(other.value()); };
    Modular(const Modular<T, M> &&other) { value_ = mod(other.value()); };
    ~Modular() {};

    T value() const {
        return value_;
    }

    bool operator< (const Modular<T, M> &other) const { 
        return value_ < other.value();
    };

    bool operator==(const Modular<T, M> &other) const { 
        return value_ == other.value();
    };

    bool operator!=(const Modular<T, M> &other) const { 
        return !(*this == other);
    };

    bool operator> (const Modular<T, M> &other) const { 
        return !(*this < other) && *this != other;
    };

    bool operator<=(const Modular<T, M> &other) const {
        return !(*this > other);   
    };

    bool operator>=(const Modular<T, M> &other) const {
        return !(*this < other);
    };

    void operator=(const Modular<T, M> &other) {
        value_ = other.value();
    };

    void operator=(const Modular<T, M> &&other) {
        value_ = other.value();
    };

    void operator+=(const Modular<T, M> &other) {
        value_ = mod(value_ + other.value());
    };

    void operator-=(const Modular<T, M> &other) {
        value_ = mod(value_ - other.value());
    };

    void operator*=(const Modular<T, M> &other) {
        value_ = mod(value_ * other.value());
    };
    void operator/=(const Modular<T, M> &other) {};
};

template <typename T, MODULO M>
Modular<T, M> operator+(const Modular<T, M> &lhs, const Modular<T, M> &rhs) {
    Modular<T, M> m(lhs);
    m += rhs;
    return m;
}

template <typename T, MODULO M>
Modular<T, M> operator-(const Modular<T, M> &lhs, const Modular<T, M> &rhs) {
    Modular<T, M> m(lhs);
    m -= rhs;
    return m;
}

template <typename T, MODULO M>
Modular<T, M> operator*(const Modular<T, M> &lhs, const Modular<T, M> &rhs) {
    Modular<T, M> m(lhs);
    m *= rhs;
    return m;
}

template <typename T, MODULO M>
Modular<T, M> operator/(const Modular<T, M> &lhs, const Modular<T, M> &rhs) {
    Modular<T, M> m;
    return m;
}

#endif /* MODULAR_ */
