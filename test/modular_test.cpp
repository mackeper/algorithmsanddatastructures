#include "modular.hpp"
#include "gtest/gtest.h"


TEST(ModularTest, DefaultConstructor)
{
    Modular<int, 100> m1;
    EXPECT_EQ(0, m1.value());
}

TEST(ModularTest, OneArgConstructor)
{
    Modular<int, 100> m1(102);
    EXPECT_EQ(2, m1.value());
    Modular<int, 100> m2(2);
    EXPECT_EQ(2, m2.value());
    Modular<int, 10> m3(0);
    EXPECT_EQ(0, m3.value());
    Modular<int, 10> m4(102);
    EXPECT_EQ(2, m4.value());
    Modular<int, 10> m5 = 103;
    EXPECT_EQ(3, m5.value());
    Modular<int, 100> m6 = -97;
    EXPECT_EQ(3, m6.value());
}

TEST(ModularTest, CopyConstructor)
{
    Modular<int, 100> m1(102);
    Modular<int, 100> m2(m1);
    EXPECT_EQ(2, m2.value());
}

TEST(ModularTest, Assignment)
{
    Modular<int, 100> m1(102);
    Modular<int, 100> m2;
    m1 = m2;
    EXPECT_EQ(0, m1.value());
}

TEST(ModularTest, Addition)
{
    {
        Modular<int, 100> m1;
        Modular<int, 100> m2;
        EXPECT_EQ(0, (m1 + m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(102);
        EXPECT_EQ(1, (m1 + m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(102);
        m2 += m1;
        EXPECT_EQ(1, (m2).value());
    }
}


TEST(ModularTest, Subtraction)
{
    {
        Modular<int, 100> m1;
        Modular<int, 100> m2;
        EXPECT_EQ(0, (m1 - m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(102);
        EXPECT_EQ(97, (m1 - m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(102);
        m2 -= m1;
        EXPECT_EQ(3, (m2).value());
    }
    {
        Modular<int, 100> m1(1);
        Modular<int, 100> m2(102);
        m2 -= m1;
        EXPECT_EQ(1, (m2).value());
    }
}


TEST(ModularTest, Multiplication)
{
    {
        Modular<int, 100> m1;
        Modular<int, 100> m2;
        EXPECT_EQ(0, (m1 * m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(102);
        EXPECT_EQ(98, (m1 * m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(102);
        m2 *= m1;
        EXPECT_EQ(98, (m2).value());
    }
    {
        Modular<int, 10> m1(-1);
        Modular<int, 10> m2(102);
        m2 *= m1;
        EXPECT_EQ(8, (m2).value());
    }
}

TEST(ModularTest, Division)
{
    {
        Modular<int, 100> m1;
        Modular<int, 100> m2(1);
        EXPECT_EQ(0, (m1 / m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(103);
        EXPECT_EQ(33, (m1 / m2).value());
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(103);
        m2 /= m1;
        EXPECT_EQ(97, (m2).value());
    }
    {
        Modular<int, 10> m1(-1);
        Modular<int, 10> m2(103);
        m2 /= m1;
        EXPECT_EQ(3, (m2).value());
    }
}

TEST(ModularTest, Comparison)
{
    {
        Modular<int, 100> m1;
        Modular<int, 100> m2(1);
        EXPECT_TRUE(m1 < m2);
    }
    {
        Modular<int, 100> m1;
        Modular<int, 100> m2(1);
        EXPECT_FALSE(m2 < m1);
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(103);
        EXPECT_TRUE(m1 > m2);
    }
    {
        Modular<int, 100> m1(99);
        Modular<int, 100> m2(99);
        EXPECT_TRUE(m1 == m2);
    }
    {
        Modular<int, 10> m1(-1);
        Modular<int, 10> m2(103);
        EXPECT_TRUE(m1 != m2);
    }
    {
        Modular<int, 10> m1(-1);
        Modular<int, 10> m2(99);
        EXPECT_TRUE(m1 >= m2);
    }
    {
        Modular<int, 10> m1(-1);
        Modular<int, 10> m2(98);
        EXPECT_TRUE(m1 >= m2);
    }
    {
        Modular<int, 10> m1(-1);
        Modular<int, 10> m2(98);
        EXPECT_TRUE(m2 <= m1);
    }
    {
        Modular<int, 10> m1(-1);
        Modular<int, 10> m2(99);
        EXPECT_TRUE(m2 <= m1);
    }
}
